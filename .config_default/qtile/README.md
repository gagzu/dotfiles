# Mi configuración de Qtile

Un administrador de ventanas de mosaico de Python puro con todas las funciones

# Caracteristicas

* Simple, pequeño y extensible. Es fácil escribir sus propios diseños, widgets y comandos.
* Configurado en Python.
* Shell de comandos que permite gestionar e inspeccionar todos los aspectos de Qtile.
* Capacidad de escritura remota completa: escriba secuencias de comandos para configurar espacios de trabajo, manipular ventanas, actualizar widgets de la barra de estado y más.
* La capacidad de escritura remota de Qtile lo convierte en uno de los gestores de ventanas más exhaustivamente probados.

# Mis combinaciones de teclas

El MODKEY está configurado en la tecla Super (también conocida como la tecla de Windows). Intento mantener las combinaciones de teclas coherentes con todos mis gestores de ventanas.

| Keybinding | Action |
| :--- | :--- |
| `MODKEY + RETURN` | abre la terminal (alacritty es la terminal pero se puede cambiar fácilmente) |
| `MODKEY + SHIFT + RETURN` | abre el iniciador de ejecución (dmenu es el iniciador de ejecución pero se puede cambiar fácilmente) |
| `MODKEY + TAB` | cambia a través de los layouts disponibles |
| `MODKEY + SHIFT + c` | cierra la ventana con foco |
| `MODKEY + SHIFT + r` | reinicia qtile |
| `MODKEY + SHIFT + q` | salir de qtile |
| `MODKEY + 1-9` | cambiar el foco al espacio de trabajo (1-9) |
| `MODKEY + SHIFT + 1-9` | enviar ventana enfocada al espacio de trabajo (1-9) |
| `MODKEY + j` | cambia el foco entre ventanas en la pila ↑ |
| `MODKEY + k` | cambia el foco entre ventanas en la pila ↓ |
| `MODKEY + SHIFT + j` | gira las ventanas en la pila ↑ |
| `MODKEY + SHIFT + k` | gira las ventanas en la pila ↓ |
| `MODKEY + h` | ampliar el tamaño de la ventana (diseño MondadTall) |
| `MODKEY + l` | reduce el tamaño de la ventana (diseño MondadTall) |
| `MODKEY + w` | cambiar el foco al monitor 1 |
| `MODKEY + e` | cambiar el foco al monitor 2 |
| `MODKEY + r` | cambiar el foco al monitor 3 |
| `MODKEY + period` | cambiar el foco al siguiente monitor |
| `MODKEY + comma` | cambiar el foco al monitor anterior |

# Comunidad

Qtile es compatible con un grupo dedicado de usuarios. Si necesita ayuda, no dude en enviar un correo electrónico a nuestra lista de correo o unirse a nosotros en IRC.

* Mailing List: http://groups.google.com/group/qtile-dev
* IRC: irc://irc.oftc.net:6667/qtile

