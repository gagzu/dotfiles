### ALIASES ###

# bare git repo alias for dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.myconfig/ --work-tree=$HOME'

# Changing "ls" to "exa"
alias ls='exa -hgal --color=always --group-directories-first' # my preferred listing
alias la='exa -hga --color=always --group-directories-first'  # all files and dirs
alias ll='exa -hgl --color=always --group-directories-first'  # long format
alias lt='exa -hgaT --color=always --group-directories-first' # tree listing

# adding flags
alias cp="cp -i"                          # confirm before overwriting something
alias dfa='df -hiT'                       # human-readable sizes type file and inodes
alias dft='df -hT'                        # human-readable sizes and type file
alias dfi='df -hi'                        # human-readable sizes and inodes
alias df='df -h'                          # human-readable sizes
alias free='free -mh'                     # show sizes in MB